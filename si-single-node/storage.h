#pragma once

#include <map>
#include <unordered_map>
#include <vector>

#include "types.h"

// MVCC storage implementation.
class Storage {
public:
  // Value is stored together with the ID of transaction which set that value.
  struct TagValue {
    TransactionId txid;
    Value value;
  };

  // Read historical value for a key, at a given timestamp.
  Value get_at(Timestamp at, Key key) const;

  // Read latest value for a key.
  const std::pair<const Timestamp, TagValue> *get_last(Key key);

  // Apply key/value write at a given timestamp.
  void apply(Timestamp ts, TransactionId txid, Key key, Value value);

  // Rollback a key/value write at a given timestamp.
  void rollback(Timestamp ts, TransactionId txid, Key key, Value value);

  typedef std::tuple<Key, Timestamp, TagValue> DumpItem;
  // Dump all data to a vector, in any order. For testing and debugging.
  void dump(std::vector<DumpItem> *out) const;

private:
  // TODO: define how data is organized inside Storage.
  // Data should be stored as a mapping: key -> history of values.
};
