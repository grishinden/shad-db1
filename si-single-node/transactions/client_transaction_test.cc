#include <cassert>

#include "client_transaction.h"
#include "discovery.h"
#include "retrier.h"

void TestClientTransactionSimple() {
  const ActorId node_id = 1;
  const ActorId client = 10;

  Discovery discovery(node_id);

  ClientTransactionSpec spec{1, // start timestamp
                             4, // commit timestamp
                             {{
                                 // Get
                                 2, // start timestamp
                                 1, // key
                             }},
                             {{
                                 // Put
                                 3,  // start timestamp
                                 2,  // key
                                 10, // value
                             }},
                             ClientTransactionSpec::COMMIT};

  RetrierNoRetries rt;
  ClientTransaction tx(client, spec, &discovery, &rt);

  std::vector<Message> in;
  std::vector<Message> out;
  tx.tick(1, in, &out);

  assert(out.size() == 1);
  assert(out[0].type == MSG_START);
  assert(out[0].destination == node_id);

  const TransactionId txid = out[0].id;

  in.push_back(CreateMessageStartAck(node_id, client, txid, 1));
  out.clear();

  tx.tick(2, in, &out);
  assert(out.size() == 0);
  in.clear();

  tx.tick(3, in, &out);

  tx.tick(4, in, &out);
  assert(out.size() == 2);

  assert(out[0].type == MSG_GET);
  assert(out[0].get<MessageGetPayload>().txid == txid);
  assert(out[0].get<MessageGetPayload>().key == 1);

  assert(out[1].type == MSG_PUT);
  assert(out[1].get<MessagePutPayload>().txid == txid);
  assert(out[1].get<MessagePutPayload>().key == 2);
  assert(out[1].get<MessagePutPayload>().value == 10);

  in.push_back(CreateMessagePutReply(node_id, client, txid, out[1].id));
  in.push_back(CreateMessageGetReply(node_id, client, txid, out[0].id, 250));
  out.clear();
  tx.tick(5, in, &out);

  assert(out.size() == 1);
  assert(out[0].type == MSG_COMMIT);
  assert(out[0].get<MessageCommitPayload>().txid == txid);

  in.clear();
  out.clear();
  in.push_back(CreateMessageCommitAck(node_id, client, txid, 5));
  tx.tick(6, in, &out);

  assert(out.size() == 0);

  assert(tx.state() == ClientTransactionState::COMMITTED);

  auto res = tx.export_results();

  assert(res.read_timestamp == 1);
  assert(res.commit_timestamp == 5);

  assert(res.gets[0].key == 1);
  assert(res.gets[0].value == 250);

  assert(res.puts[0].key == 2);
  assert(res.puts[0].value == 10);
}
