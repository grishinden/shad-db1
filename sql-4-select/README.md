# SQL Select

Мы переходим к завершающей части домашки - оператору SELECT по таблице с фильтрацией и проекцией. Мы хотим иметь возможность задавать запросы вида

```
SELECT * FROM table_name [WHERE logical_expr]
```
и
```
SELECT expr1, expr2... FROM table_name [WHERE logical_expr]
```

Здесь нам потребуется реализовать методы `createReadFromTableExecutor`, `createFilterExecutor`.

После того, как вы добавите executor и напишите внутри класса `Interpreter` логику, строящую план выполнения запроса, у вас заработают запросы из теста `sql_4_select`.
