#pragma once

#include <cstdint>
#include <vector>

#include "types.h"

struct MessageStartPayload {
  TransactionId txid;
  Timestamp read_timestamp;
};

struct MessageStartAckPayload {
  TransactionId txid;
  Timestamp read_timestamp;
};

struct MessageGetPayload {
  TransactionId txid;
  Key key;
};

struct MessageGetReplyPayload {
  TransactionId txid;
  RequestId request_id;
  Value value;
};

struct MessagePutPayload {
  TransactionId txid;
  Key key;
  Value value;
};

struct MessagePutReplyPayload {
  TransactionId txid;
  RequestId request_id;
};

struct MessageRolledBackByServerPayload {
  TransactionId txid;
  TransactionId conflict_txid;
};

struct MessagePreparePayload {
  TransactionId txid;
  Timestamp commit_timestamp;
};

struct MessagePrepareAckPayload {
  TransactionId txid;
};

struct MessageConflictPayload {
  TransactionId txid;
  TransactionId conflict_txid;
};

struct MessageCommitPayload {
  TransactionId txid;

  // Participants except the coordinator.
  std::vector<ActorId> participants;
};

struct MessageCommitAckPayload {
  TransactionId txid;
  Timestamp commit_timestamp;
};

struct MessageRollbackPayload {
  TransactionId txid;
};

struct MessageRollbackAckPayload {
  TransactionId txid;
};
