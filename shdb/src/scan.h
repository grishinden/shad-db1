#pragma once

#include "table.h"

namespace shdb
{

class ScanIterator
{
public:
    ScanIterator(std::shared_ptr<ITable> table, PageIndex page_index, RowIndex row_index)
    {
        (void)(table);
        (void)(page_index);
        (void)(row_index);
        throw std::runtime_error("Not implemented");
    }

    RowId getRowId() const { throw std::runtime_error("Not implemented"); }

    Row getRow() { throw std::runtime_error("Not implemented"); }

    Row operator*() { throw std::runtime_error("Not implemented"); }

    bool operator==(const ScanIterator & other) const
    {
        (void)(other);
        throw std::runtime_error("Not implemented");
    }

    bool operator!=(const ScanIterator & other) const
    {
        (void)(other);
        throw std::runtime_error("Not implemented");
    }

    ScanIterator & operator++() { throw std::runtime_error("Not implemented"); }

    // Your code goes here.
};


class Scan
{
public:
    explicit Scan(std::shared_ptr<ITable> table)
    {
        (void)(table);
        throw std::runtime_error("Not implemented");
    }

    ScanIterator begin() const { throw std::runtime_error("Not implemented"); }

    ScanIterator end() const { throw std::runtime_error("Not implemented"); }

    // Your code goes here.
};

}
