#pragma once

#include "schema.h"
#include "store.h"
#include "table.h"

namespace shdb
{

class Catalog
{
public:
    explicit Catalog(std::shared_ptr<Store> store)
    {
        // Your code goes here.
        (void)(store);
    }

    void saveTableSchema(const std::filesystem::path & name, std::shared_ptr<Schema> schema)
    {
        // Your code goes here.
        (void)(name);
        (void)(schema);
    }

    std::shared_ptr<Schema> findTableSchema(const std::filesystem::path & name)
    {
        // Your code goes here.
        (void)(name);
        return {};
    }

    void forgetTableSchema(const std::filesystem::path & name)
    {
        // Your code goes here.
        (void)(name);
    }

    // Your code goes here.
};

}
