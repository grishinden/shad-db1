%skeleton "lalr1.cc"
%require "2.5"
%defines
%define api.namespace {shdb}
%define api.value.type variant
%define parser_class_name {Parser}

%code requires {
    #include <memory>
    #include "ast.h"
    #include "schema.h"
    namespace shdb {class Lexer;}
}

%parse-param {shdb::Lexer& lexer} {ASTPtr & result} {std::string & message}

%code {
    #include "lexer.h"
    #define yylex lexer.lex
}

%token END 0 "end of file"
%token ERROR

%%

input: END {}

%%

void shdb::Parser::error(const std::string& err)
{
	message = err;
}
