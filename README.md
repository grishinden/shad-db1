# shad-db1

Shad Database Part 1 Course Homework

## Ссылки

- [Репозиторий с задачами](https://gitlab.com/kitaetoya/shad-db1)
- [Репозиторий с лабораторными по SQL](https://github.com/kitaisreal/DBLabs)

## Инструкции

1) [Настройка окружения](docs/setup.md)
2) [Как сдавать задачи](docs/ci.md)

## Навигация

- [Manytask](https://db1.manytask.org)
- [Результаты](https://docs.google.com/spreadsheets/d/1GBwm7bSufS2Ebs4W8L2W2PZYxucGdWYDiRhA_WeSX9U/)
