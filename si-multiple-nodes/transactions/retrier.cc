#include "retrier.h"

IRetrier::~IRetrier() {}

void RetrierNoRetries::get_ready(Timestamp ts, std::vector<Message> *messages) {
  for (auto &&msg : out_) {
    messages->push_back(msg);
  }
  out_.clear();
}

IRetrier::Handle RetrierNoRetries::schedule(Timestamp ts, Message out) {
  send_once(ts, out);
  return 1;
}

void RetrierNoRetries::send_once(Timestamp, Message out) {
  out_.push_back(out);
}

void RetrierNoRetries::cancel(Handle handle) {}

RetrierExpBackoff::RetrierExpBackoff(int seed)
    : gen_(seed), gauss_(0.5 /* mean */, 0.1 /* stddev */) {}

bool RetrierExpBackoff::EntryByTimestamp::operator()(const Entry &u,
                                                     const Entry &v) const {
  return u.ready_ts > v.ready_ts;
}

int64_t RetrierExpBackoff::backoff_time(int iteration) {
  return 1 + std::max(0.0, gauss_(gen_)) * (1 << std::min(7, iteration));
}

void RetrierExpBackoff::get_ready(Timestamp ts,
                                  std::vector<Message> *messages) {
  while (!queue_.empty() && queue_.top().ready_ts <= ts) {
    Entry entry = queue_.top();
    queue_.pop();

    auto iter = canceled_.find(entry.handle);
    if (iter == canceled_.end()) {
      messages->push_back(entry.msg);

      if (entry.handle != -1) {
        entry.ready_ts += backoff_time(entry.iteration);
        ++entry.iteration;
        queue_.push(entry);
      }
    } else {
      canceled_.erase(iter);
    }
  }
}

IRetrier::Handle RetrierExpBackoff::schedule(Timestamp ts, Message out) {
  Handle handle = next_handle_++;
  queue_.push({
      ts,
      handle,
      0,
      std::move(out),
  });

  return handle;
}

void RetrierExpBackoff::send_once(Timestamp ts, Message out) {
  queue_.push(Entry{ts,
                    -1, // handle
                    -1, // iteration
                    std::move(out)});
}

void RetrierExpBackoff::cancel(Handle handle) {
  if (handle != UNDEFINED_HANDLE) {
    canceled_.insert(handle);
  }
}
