<!DOCTYPE html>
<html lang="en">
<head>
    <title>YSDA DB Course. Lecture 2.</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="shower/themes/yandex/styles/screen-16x9.css">

    <style type="text/css">
        code { display: block; white-space: pre; background-color: #EEE; }
        p.cloud { text-align: center; line-height: 1.5; }
        p.cloud span { font-size: 13pt; color: gray; padding: 0 20px 0 20px; white-space: nowrap; }
   </style>
</head>
<body class="shower list">
    <header class="caption">
        <h1>YSDA Databases 2023</h1>
    </header>

    <section class="slide" id="cover">
        <h1 style="margin-top: 150px;">Lecture 2. Storage - 1.</h1>
    </section>

    <section class="slide">
        <h2>Plan</h2>

        <p>1. Disk-oriented database architecture.</p>
        <p>2. Storage.</p>
        <p>3. Page layout structure.</p>
        <p>4. Tuple layout structure.</p>
        <p>5. System catalog.</p>
    </section>

    <section class="slide">
        <h1>Disk-oriented database architecture</h1>

        <p>We will focus on the DBMS architecture, which assumes that the primary storage location of the database is on non-volatile storage.</p>
    </section>

    <section class="slide">
        <h1>Storage Hierarchy</h1>

        <p>At the top of the storage hierarchy there are devices closest to the CPU.
            This is the fastest storage, but it is also the smallest and most expensive.</p>
        <p>The further you move away from the CPU, the larger the storage device capacity becomes, but storage device are becoming much slower.
            These devices are also becoming cheaper per GB.</p>
        <p>Designing for high performance requires taking into account the limitations of the memory hierarchy,
            the size and capabilities of each component.</p>
        <p><a href="https://en.wikipedia.org/wiki/Memory_hierarchy">Memory hierarchy</a></p>
    </section>

    <section class="slide">
        <h1>Storage Hierarchy</h1>

        <img style="width: 100%; height:auto;" src="pictures/storage_hierarchy.png"/>
    </section>

    <secion class="slide">
        <h1>Storage Latencies</h1>

        <p>1. 0.5 nanoseconds L1 cache reference</p>
        <p>2. 7 nanoseconds L2 cache reference</p>
        <p>3. 100 nanoseconds DRAM (Can be even more if access to different NUMA node)</p>
        <p>4. 150,000 nanoseconds SSD</p>
        <p>5. 10,000,000 nanoseconds HDD</p>
        <p>6. ~30,000,000 nanoseconds Network Storage</p>
    </secion>

    <section class="slide">
        <h1>Disks measurements</h1>

        <p>Random read/write access in the case of SSD ~70 microseconds, in the case of HDD ~10-15 milliseconds</p>
        <p>Read/write throughput of SATA SSD ~600 megabytes per second, NVME SSD ~3.5 gigabytes per second.</p>

        <p><a href="https://arxiv.org/pdf/2102.11198.pdf">Reading from External Memory</a></p>
        <p><a href="https://habr.com/ru/post/154235/">https://habr.com/ru/post/154235/</a></p>
    </section>

    <section class="slide">
        <h1>Storage</h1>

        <p>DBMS components manage the movement of data between non-volatile and volatile memory.</p>
        <p>The system cannot work with data directly on disk without first moving it to volatile storage.</p>
    </section>

    <section class="slide">
        <h1>DBMS Architecture</h1>

        <p>Allow the DBMS to manage databases that exceed the amount of available memory.</p>
        <p>Reading/writing to disk is expensive, goal is to reduce reads/writes as much as possible,
            to avoid large delays and reduced performance.</p>
        <p>Prefer sequential read/write access over random read/write access.</p>
    </section>

    <section class="slide">
        <h1>DBMS Architecture</h1>

        <img style="width: 80%; height: 80%;" src="pictures/buffer_pool.png"/>
    </section>

    <section class="slide">
        <h1>OS mmap</h1>

        <p>Using the <b>mmap</b> system call. Map the contents of a file into the process address space.</p>
        <p>The operating system is responsible for data movement, to move file pages in and out of memory.</p>
    </section>

    <section class="slide">
        <h1>OS mmap</h1>

        <p><b>madvise</b>: tell the OS the subsequent page reading strategy.</p>
        <p><b>mlock</b>: tell the OS that a range of pages cannot be swapped out of memory.</p>
        <p><b>msync</b>: inform the OS to synchronize a range of pages.</p>

        <p>Many systems use this approach in whole or in part (MonetDB, LevelDB, MongoDB).</p>
    </section>

    <section class="slide">
        <h1>OS mmap</h1>

        <p>The operating system cannot effectively understand which pages need to be flushed to disk,
            which ones should be kept in memory for subsequent access.</p>

        <p>1. The correct order for flushing pages to disk.</p>
        <p>2. Preloading data (Prefetch).</p>
        <p>3. Resource IO scheduling.</p>
        <p>4. Cache policy is difficult to manage.</p>

        <p>A database management system can do this more efficiently.</p>
        <p><a href="https://db.cs.cmu.edu/papers/2022/cidr2022-p13-crotty.pdf">Are You Sure You Want to Use MMAP in Your Database Management System?</a></p>
    </section>

    <section class="slide">
        <h1>File Storage</h1>

        <p>The DBMS stores the database in the form of one or more files on disk. The OS knows nothing about the contents of these files.</p>
        <p>Early systems from the 1980s used custom file systems to interact with raw devices.</p>
        <p>Some enterprise DBMSs still support this (Oracle).</p>
        <p>Most new DBMSs do not do this.</p>
    </section>

    <section class="slide">
        <h1>Storage Manager</h1>

        <p>Storage Manager is responsible for storing database files.</p>

        <p>1. Organize files as a set of pages.</p>
        <p>2. Tracking reading/writing data to pages.</p>
        <p>3. Monitoring of available space.</p>
        <p>4. May be responsible for scheduling reads and writes. Improve data locality on pages.</p>
    </section>

    <section class="slide">
        <h1>DBMS page</h1>

        <p>A page is a block of data of a fixed size (Usually 512B - 16KB).</p>
        <p>It can contain tuples, metadata, indexes, WAL records.</p>
        <p>Each page is assigned a unique identifier.</p>
        <p>The DBMS uses an indirect layer to map page identifiers to
            their physical locations on disk.</p>
    </section>

    <section class="slide">
        <h1>Page Storage Architecture</h1>

        <p>DBMS organize the storage of pages in files on disk in different ways.</p>
        <p>Interface to <b>create</b>, <b>read</b>, <b>write</b>, <b>delete</b> page.</p>
        <p>Has metadata to keep track of which pages exist and which have free space.</p>

        <p>1. Heap File</p>
        <p>2. Sequential / Sorted file Organization</p>
        <p>3. Hashing File Organization</p>

        <p><a href="https://en.wikipedia.org/wiki/Database_engine">Database engine</a></p>
        <p><a href="https://en.wikipedia.org/wiki/Database_storage_structures">Database storage structures</a></p>
    </section>

    <section class="slide">
        <h1>Heap file</h1>

        <p>This is an unordered set of pages where tuples are stored in random order.</p>
        <p>1. Linked List</p>
        <p>2. Page Directory</p>
    </section>

    <section class="slide">
        <h1>Linked List</h1>

        <p>HEADER page at the beginning of the file, which stores two pointers:</p>
        <p>1. Head (HEAD) of the list of free pages.</p>
        <p>2. Head (HEAD) of the list of pages filled with data.</p>

        <p>Each page itself tracks the number of free slots.</p>
        <p>In the case of variable length, you cannot have completely filled pages.</p>
    </section>

    <section class="slide">
        <h1>Linked List</h1>

        <img style="width: 80%; height: 80%;" src="pictures/linked_list.png"/>
    </section>

    <section class="slide">
        <h1>Page Directory</h1>

        <p>The DBMS supports special pages that track the location of data pages in database files.</p>
        <p>The catalog also records the number of free slots on the page.</p>
        <p>The DBMS must ensure that the catalog pages are synchronized with the data pages.</p>
    </section>

    <section class="slide">
        <h1>Page Directory</h1>

        <img style="width: 80%; height: 80%;" src="pictures/page_directory.png"/>
    </section>

    <section class="slide">
        <h1>Page layout</h1>

        <p>Each page contains a header with metadata about the page content.</p>

        <img style="width: 40%; height: auto; float: right; margin-top: -20px;" src="pictures/page_header.png"/>
        <p>1. Page size</p>
        <p>2. Checksum</p>
        <p>3. DBMS version</p>
        <p>4. Transaction visibility</p>
        <p>5. Compression information</p>
    </section>

    <section class="slide">
        <h1>Page layout</h1>

        <p>Now for any page storage architecture we need to understand how to organize the data stored inside
            pages. We still assume that we are only storing tuples.</p>
        <p>1. Tuple-oriented</p>
        <p>2. Log-structured</p>
    </section>

    <section class="slide">
        <h1>Tuple storage</h1>

        <p>We store the number of tuples in the header. We write tuples after the header.</p>
        <img style="width: 100%; height: auto;" src="pictures/tuple_page.png"/>
    </section>

    <section class="slide">
        <h1>Slotted pages</h1>

        <p>We store an array of free slots in the page, each slot stores a pointer to the beginning of the tuple record and its length.
            This is used by almost all classical DBMS.</p>

        <img style="width: 90%; height: 60%;" src="pictures/slotted_pages.png"/>
    </section>

    <section class="slide">
        <h1>Tuple layout</h1>

        <p>A sequence of bytes, the DBMS task is to interpret it.</p>
        <p>DBMS system catalogs contain information about the table schema that the system uses to determine tuple attribute types.</p>
        <p>Has a header with metadata:</p>
        <p>1. Transaction visibility metadata.</p>
        <p>2. Bit mask for NULL values.</p>

        <img style="width: 90%; height: auto;" src="pictures/tuple_header.png"/>
    </section>

    <section class="slide">
        <h1>Record identifiers</h1>

        <p>The DBMS needs a way to keep track of individual tuples. Each tuple is assigned a unique record ID.</p>
        <p>Usually this is page_id + offset/slot. May also contain information about the location of the file.</p>
    </section>

    <section class="slide">
        <h1>Record identifiers example</h1>

        <p>PostgreSQL CTID record identifier.</p>
        <code style="font-size:14pt;">CREATE TABLE test_table (id INTEGER, value VARCHAR);
SELECT test_table.ctid, * FROM test_table;

SELECT * FROM test_table WHERE ctid = '(0,1)';
 id | value
----+-------
  0 | Value
(1 row)
        </code>
    </section>

    <secion class="slide">
        <h1>Integers</h1>

        <p>Most DBMSs store integers using their "native" C/C++ types, as specified in the IEEE-754 standard. These values have a fixed length.</p>
        <p><a href="https://en.wikipedia.org/wiki/IEEE_754">https://en.wikipedia.org/wiki/IEEE_754</a></p>
        <p>Examples: INTEGER, BIGINT, SMALLINT, TINYINT.</p>
    </secion>

    <secion class="slide">
        <h1>Variable Precision Numbers</h1>

        <p>Floating-point numbers using the "native" C/C++ types specified in the IEEE-754 standard. These values have a fixed length.</p>
        <p>Floating-point numbers are faster than arbitrary-precision numbers because the CPU can directly execute instructions on them.</p>
        <p><a href="https://en.wikipedia.org/wiki/IEEE_754">https://en.wikipedia.org/wiki/IEEE_754</a></p>
        <p>Examples: FLOAT, DOUBLE/REAL.</p>
    </secion>

    <secion class="slide">
        <h1>Fixed Precision Numbers</h1>

        <p>These are numeric data types with arbitrary precision and scale. They are usually stored in an exact variable-length binary representation
            with additional metadata (scale, precision) that tells the system, for example, where the decimal place should be.</p>
        <p>These data types are used when rounding errors are unacceptable, but the DBMS pays a performance cost to obtain such precision.</p>
        <p>Examples: NUMERIC, DECIMAL.</p>
    </secion>

    <secion class="slide">
        <h1>Fixed Length Data</h1>

        <p>Fixed-length byte array.</p>
        <p>If the value takes up less than the length, the free space is not used.</p>
        <p>Examples: CHAR.</p>
    </secion>

    <secion class="slide">
        <h1>Variable Length Data</h1>

        <p>Byte array of arbitrary length.</p>
        <p>Has a header that keeps track of the length of the string to make it easier to move to the next value.</p>
        <p>Most DBMSs do not allow a tuple to be larger than one page, so they solve this problem by
            moving value to the overflow page, and place in the tuple a pointer to the value on overflow page.</p>
        <p>Examples: VARCHAR, VARBINARY, TEXT.</p>
    </secion>

    <secion class="slide">
        <h1>Variable Length Data</h1>

        <p>Most DBMSs do not allow a tuple to be larger than one page, so they solve this problem by
            moving value to the overflow page, and place in the tuple a pointer to the value on overflow page.</p>
        <p>Postgres: TOAST (>1/4 size of page 2KB)</p>
        <p>MySQL: Overflow (>1⁄2 size of page)</p>

        <p><a href="https://www.postgresql.org/docs/current/storage-toast.html">PostgreSQL storage TOAST</a></p>
    </secion>

    <secion class="slide">
        <h1>Variable Length Data Overflow</h1>

        <img style="width: 85%; height: auto;" src="pictures/tuple_overflow_page.png"/>
    </secion>

    <secion class="slide">
        <h1>External Value Data</h1>

        <p>Some systems allow to store large values in an external file, in which case the tuple will contain a pointer to that file.
            The the disadvantage of this is that the DBMS cannot manipulate the contents of this file.</p>
        <p>No transactional support.</p>
        <p>Examples: BLOB.</p>

        <p><a href="https://arxiv.org/pdf/cs/0701168.pdf">To BLOB or Not To BLOB: Large Object Storage in a Database or a Filesystem?</a></p>
    </secion>

    <secion class="slide">
        <h1>External Vength Data</h1>

        <img style="width: 85%; height: auto;" src="pictures/tuple_blob_page.png"/>
    </secion>


    <secion class="slide">
        <h1>Dates and Times</h1>

        <p>They are usually represented as the number of (micro/milli) seconds since the UNIX epoch (UNIX timestamp).</p>
        <p>Examples: TIME, DATE, TIMESTAMP.</p>
    </secion>

    <section class="slide">
        <h1>Additional types</h1>

        <p>Many DBMSs introduce additional types.</p>
         <p>1. ENUM</p>
         <p>2. Arrays</p>
         <p>3. Geometric types (POINT, POLYGON)</p>
         <p>4. Composite types</p>
         <p>5. JSON/XML</p>
         <p>6. Range</p>
    </section>

    <section class="slide">
        <h1>System catalog</h1>

        <p>The DBMS stores metadata about databases in its system directories.</p>
        <p>1. Tables, views, columns, attributes, indexes.</p>
        <p>2. Users, access rights.</p>

        <p>Most DBMSs store metadata in special system tables.</p>
    </section>

    <section class="slide">
        <h1>System catalog</h1>

        <p>In relational databases, an information schema (information_schema) is a set of read-only views, according to ANSI standard,
            which provide information about all tables, views, columns and procedures in the database.</p>
        <p>Also most databases provide system specific system catalogs.</p>

        <p><a href="https://en.wikipedia.org/wiki/Information_schema">https://en.wikipedia.org/wiki/Information_schema</a></p>
    </section>

    <section class="slide">
        <h1>System catalog</h1>

        <p>Most DBMSs provide additional information using non-standard commands</p>
        <p>MySQL - SHOW</p>
        <p>Oracle - DESCRIBE</p>
        <p>psql (Postgres default command-line program) - \d</p>
    </section>

    <section class="slide">
        <h1>System catalog example</h1>

        <code style="font-size:14pt;">SELECT table_catalog, table_schema, table_name, table_type
FROM information_schema.tables LIMIT 3;

 table_catalog | table_schema |    table_name    | table_type
---------------+--------------+------------------+------------
 test_db       | pg_catalog   | pg_statistic     | BASE TABLE
 test_db       | pg_catalog   | pg_type          | BASE TABLE
 test_db       | pg_catalog   | pg_foreign_table | BASE TABLE
(3 rows)
        </code>
    </section>

    <section class="slide">
        <h1>System catalog example</h1>

        <code style="font-size:14pt;">CREATE TABLE test_table (id INTEGER, value VARCHAR, PRIMARY KEY (id));

SELECT table_catalog, table_schema, table_name, table_type
FROM information_schema.tables LIMIT 3;

 table_catalog | table_schema |  table_name  | table_type
---------------+--------------+--------------+------------
 test_db       | public       | test_table   | BASE TABLE
 test_db       | pg_catalog   | pg_statistic | BASE TABLE
 test_db       | pg_catalog   | pg_type      | BASE TABLE
(3 rows)
        </code>
    </section>

    <section class="slide">
        <h1>System catalog example</h1>

        <code style="font-size:14pt;">SELECT column_name, data_type, is_nullable
FROM information_schema.columns WHERE table_name = 'test_table';

 column_name |     data_type     | is_nullable
-------------+-------------------+-------------
 id          | integer           | NO
 value       | character varying | YES
(2 rows)
        </code>
    </section>

    <section class="slide">
        <h1>Conclusion</h1>

        <p>Designing for high performance requires taking into account the limitations of the memory hierarchy,
            the size and capabilities of each component.</p>
        <p>Main goal of disk-oriented architecture is to allow the DBMS to manage databases that exceed the amount of available memory.</p>
        <p>There are a lot of different storage architectures that provide different trade-offs.</p>
    </section>

    <section class="slide">
        <h1>Questions ?</h1>
    </section>

    <div class="progress"></div>
    <script src="shower/shower.min.js"></script>

    <!--Video plugin-->
    <link rel="stylesheet" href="shower/shower-video.css">
    <script src="shower/shower-video.js"></script>
    <!--/Video plugin-->
</body>
</html>
